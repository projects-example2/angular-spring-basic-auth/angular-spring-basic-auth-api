package com.example.basicauthapi.controller;

import com.example.basicauthapi.model.Employee;
import com.example.basicauthapi.model.User;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/employees")
@CrossOrigin
public class EmployeeController {
    private List<Employee> employees = createList();
    @GetMapping(path = "/validateLogin" , produces = "application/json")
    public User validateLogin() {
        User user = new User();
        user.setStatus("User successfully authenticated");
        return  user;
    }
    @GetMapping("")
    public List<Employee> firstPage() {
        return employees;
    }
    @DeleteMapping("/{id}")
    public Employee delete(@PathVariable("id") int id) {
        Employee deletedEmp = null;
        for (Employee emp : employees) {
            if (emp.getEmpId().equals(id)) {
                employees.remove(emp);
                deletedEmp = emp;
                break;
            }
        }
        return deletedEmp;
    }
    @PostMapping
    public Employee create(@RequestBody Employee user) {
        employees.add(user);
        return user;
    }
    private static List<Employee> createList() {
        List<Employee> tempEmployees = new ArrayList<>();
        Employee emp1 = new Employee();
        emp1.setName("emp1");
        emp1.setDesignation("manager");
        emp1.setEmpId("1");
        emp1.setSalary(3000);

        Employee emp2 = new Employee();
        emp2.setName("emp2");
        emp2.setDesignation("developer");
        emp2.setEmpId("2");
        emp2.setSalary(3000);
        tempEmployees.add(emp1);
        tempEmployees.add(emp2);
        return tempEmployees;
    }

}
